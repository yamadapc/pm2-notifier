var notifier = require('node-notifier');
var pm2 = require('pm2');

pm2.launchBus(function(err, bus) {
  bus.on('process:event', function(data) {
    console.log(data.event);
    notifier.notify({
      title: 'Process ' + data.event,
      message: 'Process ' + data.process.name + messageForEvent(data),
    });
  });
});

function messageForEvent(data) {
  switch(data.event) {
    case 'online':
      return ' is online';
    case 'exit':
      return ' exited';
    default:
      return ' - Event: ' + data.event;
  }
}
